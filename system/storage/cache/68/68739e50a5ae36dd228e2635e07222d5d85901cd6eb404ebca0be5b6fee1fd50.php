<?php

/* __string_template__64d5d6f7030814a1bb0baf239403849405c5851ee5a7336169769a0d9ef0af78 */
class __TwigTemplate_46ba6fce73897bc72a85e981259691a31902e22fa0f3031fd85f1b771d21bf74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 2
            echo "
<section id=\"contect\">
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-3\">
      <div class=\"categories\">
        <h3> الفئات</h3>
        <ul>
      <a href=\"#\" class=\"collapsed-btn\" data-toggle=\"collapse\" data-target=\"#demo\"> <li> اكسسوارات </li>  <span ><i class=\"fas fa-angle-left\"></i></span></a>
      <div class=\"collapse\" id=\"demo\">
        <ul class=\"sub\">
          <a href=\"#\"> <li> خواتم </li> </a> 
          <a href=\"#\"> <li> خواتم </li> </a> 
          <a href=\"#\"> <li> خواتم </li> </a> 
          <a href=\"#\"> <li> خواتم </li> </a> 
          <a href=\"#\"> <li> خواتم </li> </a> 
         
        </ul>
        </div>
        <li> <a href=\"#\"> اكسسوارات   <span><i class=\"fas fa-angle-left\"></i></span></a>
        </li>
        <li> <a href=\"#\"> اكسسوارات   <span><i class=\"fas fa-angle-left\"></i></span></a>
        </li>
        <li> <a href=\"#\"> اكسسوارات   <span><i class=\"fas fa-angle-left\"></i></span></a>
        </li>
     <li> <a href=\"#\"> اكسسوارات   <span><i class=\"fas fa-angle-left\"></i></span></a>
        </li>
        
      </ul>
<h3> السعر</h3>
<form method=\"post\" action=\"/action_page_post.php\">
  <input type=\"text\" class=\"js-range-slider\" name=\"my_range\" value=\"\" />
 </form>
      </div>
    </div>
    </div>
    <div>

  </section>


    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
            // line 43
            echo (isset($context["text_category"]) ? $context["text_category"] : null);
            echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
      <ul class=\"nav navbar-nav\">
        ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 49
                echo "        ";
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 50
                    echo "        <li class=\"dropdown\"><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
          <div class=\"dropdown-menu\">
            <div class=\"dropdown-inner\"> ";
                    // line 52
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), (twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array())) / twig_round($this->getAttribute($context["category"], "column", array()), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 53
                        echo "              <ul class=\"list-unstyled\">
                ";
                        // line 54
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 55
                            echo "                <li><a href=\"";
                            echo $this->getAttribute($context["child"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["child"], "name", array());
                            echo "</a></li>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 57
                        echo "              </ul>
              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 58
                    echo "</div>
            <a href=\"";
                    // line 59
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"see-all\">";
                    echo (isset($context["text_all"]) ? $context["text_all"] : null);
                    echo " ";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a> </div>
        </li>
        ";
                } else {
                    // line 62
                    echo "        <li><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
        ";
                }
                // line 64
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "      </ul>
    </div>

";
        }
        // line 68
        echo " ";
    }

    public function getTemplateName()
    {
        return "__string_template__64d5d6f7030814a1bb0baf239403849405c5851ee5a7336169769a0d9ef0af78";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 68,  143 => 65,  137 => 64,  129 => 62,  119 => 59,  116 => 58,  109 => 57,  98 => 55,  94 => 54,  91 => 53,  87 => 52,  79 => 50,  76 => 49,  72 => 48,  64 => 43,  21 => 2,  19 => 1,);
    }
}
/* {% if categories %}*/
/* */
/* <section id="contect">*/
/* <div class="container">*/
/*   <div class="row">*/
/*     <div class="col-md-3">*/
/*       <div class="categories">*/
/*         <h3> الفئات</h3>*/
/*         <ul>*/
/*       <a href="#" class="collapsed-btn" data-toggle="collapse" data-target="#demo"> <li> اكسسوارات </li>  <span ><i class="fas fa-angle-left"></i></span></a>*/
/*       <div class="collapse" id="demo">*/
/*         <ul class="sub">*/
/*           <a href="#"> <li> خواتم </li> </a> */
/*           <a href="#"> <li> خواتم </li> </a> */
/*           <a href="#"> <li> خواتم </li> </a> */
/*           <a href="#"> <li> خواتم </li> </a> */
/*           <a href="#"> <li> خواتم </li> </a> */
/*          */
/*         </ul>*/
/*         </div>*/
/*         <li> <a href="#"> اكسسوارات   <span><i class="fas fa-angle-left"></i></span></a>*/
/*         </li>*/
/*         <li> <a href="#"> اكسسوارات   <span><i class="fas fa-angle-left"></i></span></a>*/
/*         </li>*/
/*         <li> <a href="#"> اكسسوارات   <span><i class="fas fa-angle-left"></i></span></a>*/
/*         </li>*/
/*      <li> <a href="#"> اكسسوارات   <span><i class="fas fa-angle-left"></i></span></a>*/
/*         </li>*/
/*         */
/*       </ul>*/
/* <h3> السعر</h3>*/
/* <form method="post" action="/action_page_post.php">*/
/*   <input type="text" class="js-range-slider" name="my_range" value="" />*/
/*  </form>*/
/*       </div>*/
/*     </div>*/
/*     </div>*/
/*     <div>*/
/* */
/*   </section>*/
/* */
/* */
/*     <div class="navbar-header"><span id="category" class="visible-xs">{{ text_category }}</span>*/
/*       <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>*/
/*     </div>*/
/*     <div class="collapse navbar-collapse navbar-ex1-collapse">*/
/*       <ul class="nav navbar-nav">*/
/*         {% for category in categories %}*/
/*         {% if category.children %}*/
/*         <li class="dropdown"><a href="{{ category.href }}" class="dropdown-toggle" data-toggle="dropdown">{{ category.name }}</a>*/
/*           <div class="dropdown-menu">*/
/*             <div class="dropdown-inner"> {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}*/
/*               <ul class="list-unstyled">*/
/*                 {% for child in children %}*/
/*                 <li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/*                 {% endfor %}*/
/*               </ul>*/
/*               {% endfor %}</div>*/
/*             <a href="{{ category.href }}" class="see-all">{{ text_all }} {{ category.name }}</a> </div>*/
/*         </li>*/
/*         {% else %}*/
/*         <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/* */
/* {% endif %} */
