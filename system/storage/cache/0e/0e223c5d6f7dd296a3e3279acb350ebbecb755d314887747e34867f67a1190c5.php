<?php

/* __string_template__2deb6361a13a4f8b53458e0eec1ff85e20afd8045ea72bb862fdc638225e1198 */
class __TwigTemplate_d06f438bd48ef073fdf9a32ad286fbf6a4b1df3b9b6d9798356456e14436e378 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
   <form id=\"search\" class=\"from-group\">
              <input  type=\"text\" name=\"search\" value=\"";
        // line 3
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "\" class=\"form-control\" placeholder=\"ابحث الان عن المنتجات\">
             <button type=\"button\" class=\"btn\" > <i class=\"fas fa-search\"></i> </button> 
        
     </form>";
    }

    public function getTemplateName()
    {
        return "__string_template__2deb6361a13a4f8b53458e0eec1ff85e20afd8045ea72bb862fdc638225e1198";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
/* */
/*    <form id="search" class="from-group">*/
/*               <input  type="text" name="search" value="{{ search }}" class="form-control" placeholder="ابحث الان عن المنتجات">*/
/*              <button type="button" class="btn" > <i class="fas fa-search"></i> </button> */
/*         */
/*      </form>*/
