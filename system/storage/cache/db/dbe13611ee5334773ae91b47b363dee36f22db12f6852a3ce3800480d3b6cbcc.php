<?php

/* __string_template__d777aa3a9deb436f9066813a7065ed0b5b4daad3cb39fe3c8e393af6753eafb9 */
class __TwigTemplate_26c22e53f62c1b90e903fec6ca813e077334eb5a2a1ac9d5021cf546cd60a95b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 13
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 14
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 15
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
";
        }
        // line 17
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 18
            echo "<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
";
        }
        // line 20
        echo "
      <link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
      <link href=\"https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700\" rel=\"stylesheet\">
      <link href=\"catalog/view/javascript/css/all.min.css\" type=\"stylesheet\">
      <link href=\"catalog/view/javascript/css/fontawesome.min.css\" type=\"stylesheet\">

      <!-- light owl -->
 
      <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css\"/>
      <link rel=\"stylesheet\" href=\"catalog/view/javascript/css/owl.carousel.min.css\" type=\"text/css\">
      <link href=\"catalog/view/javascript/css/animate.css\" rel=\"stylesheet\">
   
   
      <!-- BootStrap StyleSheets -->

      <link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/javascript/css/bootstrap.min.css\">
      <link rel=\"stylesheet\" href=\"https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css\" integrity=\"sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz\" crossorigin=\"anonymous\">
    
      <link href=\"catalog/view/theme/default/stylesheet/style.css\" rel=\"stylesheet\">
      <link href=\"catalog/view/javascript/css/media.css\" rel=\"stylesheet\">
              
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/jquery.min.js\"></script>
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/bootstrap.min.js\"></script>
       <script src=\"https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js\"></script>
        <script type=\"text/javascript\" src=\"catalog/view/javascript/js/fontawesome-all.min.js\"></script>
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/owl.carousel.min.js\"></script>
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/wow.min.js\"></script>
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/all.min.js\"></script>
       <script src=\"https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js\" integrity=\"sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH\" crossorigin=\"anonymous\"></script>
       <script type=\"text/javascript\" src=\"catalog/view/javascript/js/main.js\"></script>

";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 52
            echo "<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 55
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 59
            echo "<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 62
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "</head>
<body>
 
     <header class=\"header fixed\" id=\"up\" >
        <div class=\"container\">
            <div class=\"row\">
                     <nav class=\"navbar navbar-expand-lg navbar-light \">
                       <div class=\"mobile\">
                        <div class=\"nav-toggle\">
                            <i class=\"fas fa-bars\"></i>
                            <i class=\"fas fa-times\"></i>
                     </div>

                     <div class=\"col-sm-12 d-lg-none d-md-block\">
                                        
                        <ul  class=\"\">
                        <div class=\"account\">
                          <a href=\"#\">
                            <i class=\"fas fa-user-circle\"></i>
                            حسابي 
              
                          </a>
                        </div>
                        <div class=\"option\">
                           <a href=\"#\"> <i class=\"far fa-heart\"></i> </a>
                           <a href=\"cart.html\"> <i class=\"fas fa-shopping-cart\"></i> </a>
                          <a href=\"#\"> <span> EN</span> </a>
                        </div>
                          <a class=\"navbar-brand \" href=\"index.html\"> <img class=\" logo\">logo</a>
                          </ul>
                        
                   
                      </div>
                    </div>
                      <div class=\"col-lg-8 col-xl-9\">
                      <div   class=\"mysidenav\" id=\"mysidenav\">
                                 
                                        <ul class=\"navbar-nav\">
                                            <li class=\"nav-item active \">
                                              <a class=\"nav-link  scroll\" href=\"index.html\">الرئيسيه </a>
                                            </li>
                                            <li class=\"nav-item\">
                                              <a class=\"nav-link scroll\" href=\"#\">البحث</a>
                                            </li>
                                            <li class=\"nav-item\">
                                              <a class=\"nav-link scroll\" href=\"#\">احسن العروض </a>
                                            </li>
                                            <li class=\"nav-item\">
                                              <a class=\"nav-link scroll\" href=\"#\">من نحن </a>
                                            </li>
                                            <li class=\"nav-item\">
                                                <a class=\"nav-link scroll\" href=\"#\">اتصل بنا</a>
                                            </li>
                                        
                                          </ul>
                                   
                                    </div>
                      </div>
               
                            <div class=\"col-lg-4 d-none d-lg-block\">
                                        
                                <ul  class=\"navbar-nav\">
                                <div class=\"account\">
                                  <a href=\"#\">
                                    <i class=\"fas fa-user-circle\"></i>
                                    حسابي 
                      
                                  </a>
                                </div>
                                <div class=\"option\">
                                   <a href=\"#\"> <i class=\"far fa-heart\"></i> </a>
                                   <a href=\"cart.html\"> <i class=\"fas fa-shopping-cart\"></i> </a>
                                  <a href=\"#\"> <span> EN</span> </a>
                                </div>
                                  <a class=\"navbar-brand \" href=\"index.html\"> <img class=\" logo\">logo</a>
                                  </ul>
                                
                           
                              </div>
                            
                            </nav>
                          </div>
                        </div>
                   
            </header>

<nav id=\"top\">
  <div class=\"container\">";
        // line 151
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "
    ";
        // line 152
        echo (isset($context["language"]) ? $context["language"] : null);
        echo "
    <div id=\"top-links\" class=\"nav pull-right\">
      <ul class=\"list-inline\">
        <li><a href=\"";
        // line 155
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\"><i class=\"fa fa-phone\"></i></a> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "</span></li>
        <li class=\"dropdown\"><a href=\"";
        // line 156
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</span> <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu dropdown-menu-right\">
            ";
        // line 158
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 159
            echo "            <li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 160
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 161
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 162
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 163
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
            ";
        } else {
            // line 165
            echo "            <li><a href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
            <li><a href=\"";
            // line 166
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
            ";
        }
        // line 168
        echo "          </ul>
        </li>
        <li><a href=\"";
        // line 170
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\" id=\"wishlist-total\" title=\"";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "\"><i class=\"fa fa-heart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</span></a></li>
        <li><a href=\"";
        // line 171
        echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
        echo "\" title=\"";
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "</span></a></li>
        <li><a href=\"";
        // line 172
        echo (isset($context["checkout"]) ? $context["checkout"] : null);
        echo "\" title=\"";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "\"><i class=\"fa fa-share\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-sm-5\">
        <div id=\"logo\">";
        // line 181
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            echo "<a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive\" /></a>";
        } else {
            // line 182
            echo "          <h1><a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "</a></h1>
          ";
        }
        // line 183
        echo "</div>
      </div>
       <div class=\"col-sm-3\">";
        // line 185
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "</div> 
      <div class=\"col-sm-3\">";
        // line 186
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "</div>
    </div>
  </div>
</header>
";
        // line 190
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "__string_template__d777aa3a9deb436f9066813a7065ed0b5b4daad3cb39fe3c8e393af6753eafb9";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  397 => 190,  390 => 186,  386 => 185,  382 => 183,  374 => 182,  362 => 181,  346 => 172,  338 => 171,  330 => 170,  326 => 168,  319 => 166,  312 => 165,  305 => 163,  299 => 162,  293 => 161,  287 => 160,  280 => 159,  278 => 158,  269 => 156,  263 => 155,  257 => 152,  253 => 151,  164 => 64,  156 => 62,  152 => 61,  141 => 59,  137 => 58,  134 => 57,  125 => 55,  121 => 54,  108 => 52,  104 => 51,  71 => 20,  65 => 18,  63 => 17,  57 => 15,  55 => 14,  51 => 13,  47 => 12,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if description %}*/
/* <meta name="description" content="{{ description }}" />*/
/* {% endif %}*/
/* {% if keywords %}*/
/* <meta name="keywords" content="{{ keywords }}" />*/
/* {% endif %}*/
/* */
/*       <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*       <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">*/
/*       <link href="catalog/view/javascript/css/all.min.css" type="stylesheet">*/
/*       <link href="catalog/view/javascript/css/fontawesome.min.css" type="stylesheet">*/
/* */
/*       <!-- light owl -->*/
/*  */
/*       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>*/
/*       <link rel="stylesheet" href="catalog/view/javascript/css/owl.carousel.min.css" type="text/css">*/
/*       <link href="catalog/view/javascript/css/animate.css" rel="stylesheet">*/
/*    */
/*    */
/*       <!-- BootStrap StyleSheets -->*/
/* */
/*       <link rel="stylesheet" type="text/css" href="catalog/view/javascript/css/bootstrap.min.css">*/
/*       <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz" crossorigin="anonymous">*/
/*     */
/*       <link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">*/
/*       <link href="catalog/view/javascript/css/media.css" rel="stylesheet">*/
/*               */
/*        <script type="text/javascript" src="catalog/view/javascript/js/jquery.min.js"></script>*/
/*        <script type="text/javascript" src="catalog/view/javascript/js/bootstrap.min.js"></script>*/
/*        <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>*/
/*         <script type="text/javascript" src="catalog/view/javascript/js/fontawesome-all.min.js"></script>*/
/*        <script type="text/javascript" src="catalog/view/javascript/js/owl.carousel.min.js"></script>*/
/*        <script type="text/javascript" src="catalog/view/javascript/js/wow.min.js"></script>*/
/*        <script type="text/javascript" src="catalog/view/javascript/js/all.min.js"></script>*/
/*        <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH" crossorigin="anonymous"></script>*/
/*        <script type="text/javascript" src="catalog/view/javascript/js/main.js"></script>*/
/* */
/* {% for style in styles %}*/
/* <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* <script src="catalog/view/javascript/common.js" type="text/javascript"></script>*/
/* {% for link in links %}*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endfor %}*/
/* {% for analytic in analytics %}*/
/* {{ analytic }}*/
/* {% endfor %}*/
/* </head>*/
/* <body>*/
/*  */
/*      <header class="header fixed" id="up" >*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                      <nav class="navbar navbar-expand-lg navbar-light ">*/
/*                        <div class="mobile">*/
/*                         <div class="nav-toggle">*/
/*                             <i class="fas fa-bars"></i>*/
/*                             <i class="fas fa-times"></i>*/
/*                      </div>*/
/* */
/*                      <div class="col-sm-12 d-lg-none d-md-block">*/
/*                                         */
/*                         <ul  class="">*/
/*                         <div class="account">*/
/*                           <a href="#">*/
/*                             <i class="fas fa-user-circle"></i>*/
/*                             حسابي */
/*               */
/*                           </a>*/
/*                         </div>*/
/*                         <div class="option">*/
/*                            <a href="#"> <i class="far fa-heart"></i> </a>*/
/*                            <a href="cart.html"> <i class="fas fa-shopping-cart"></i> </a>*/
/*                           <a href="#"> <span> EN</span> </a>*/
/*                         </div>*/
/*                           <a class="navbar-brand " href="index.html"> <img class=" logo">logo</a>*/
/*                           </ul>*/
/*                         */
/*                    */
/*                       </div>*/
/*                     </div>*/
/*                       <div class="col-lg-8 col-xl-9">*/
/*                       <div   class="mysidenav" id="mysidenav">*/
/*                                  */
/*                                         <ul class="navbar-nav">*/
/*                                             <li class="nav-item active ">*/
/*                                               <a class="nav-link  scroll" href="index.html">الرئيسيه </a>*/
/*                                             </li>*/
/*                                             <li class="nav-item">*/
/*                                               <a class="nav-link scroll" href="#">البحث</a>*/
/*                                             </li>*/
/*                                             <li class="nav-item">*/
/*                                               <a class="nav-link scroll" href="#">احسن العروض </a>*/
/*                                             </li>*/
/*                                             <li class="nav-item">*/
/*                                               <a class="nav-link scroll" href="#">من نحن </a>*/
/*                                             </li>*/
/*                                             <li class="nav-item">*/
/*                                                 <a class="nav-link scroll" href="#">اتصل بنا</a>*/
/*                                             </li>*/
/*                                         */
/*                                           </ul>*/
/*                                    */
/*                                     </div>*/
/*                       </div>*/
/*                */
/*                             <div class="col-lg-4 d-none d-lg-block">*/
/*                                         */
/*                                 <ul  class="navbar-nav">*/
/*                                 <div class="account">*/
/*                                   <a href="#">*/
/*                                     <i class="fas fa-user-circle"></i>*/
/*                                     حسابي */
/*                       */
/*                                   </a>*/
/*                                 </div>*/
/*                                 <div class="option">*/
/*                                    <a href="#"> <i class="far fa-heart"></i> </a>*/
/*                                    <a href="cart.html"> <i class="fas fa-shopping-cart"></i> </a>*/
/*                                   <a href="#"> <span> EN</span> </a>*/
/*                                 </div>*/
/*                                   <a class="navbar-brand " href="index.html"> <img class=" logo">logo</a>*/
/*                                   </ul>*/
/*                                 */
/*                            */
/*                               </div>*/
/*                             */
/*                             </nav>*/
/*                           </div>*/
/*                         </div>*/
/*                    */
/*             </header>*/
/* */
/* <nav id="top">*/
/*   <div class="container">{{ currency }}*/
/*     {{ language }}*/
/*     <div id="top-links" class="nav pull-right">*/
/*       <ul class="list-inline">*/
/*         <li><a href="{{ contact }}"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md">{{ telephone }}</span></li>*/
/*         <li class="dropdown"><a href="{{ account }}" title="{{ text_account }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_account }}</span> <span class="caret"></span></a>*/
/*           <ul class="dropdown-menu dropdown-menu-right">*/
/*             {% if logged %}*/
/*             <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/*             <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/*             <li><a href="{{ transaction }}">{{ text_transaction }}</a></li>*/
/*             <li><a href="{{ download }}">{{ text_download }}</a></li>*/
/*             <li><a href="{{ logout }}">{{ text_logout }}</a></li>*/
/*             {% else %}*/
/*             <li><a href="{{ register }}">{{ text_register }}</a></li>*/
/*             <li><a href="{{ login }}">{{ text_login }}</a></li>*/
/*             {% endif %}*/
/*           </ul>*/
/*         </li>*/
/*         <li><a href="{{ wishlist }}" id="wishlist-total" title="{{ text_wishlist }}"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_wishlist }}</span></a></li>*/
/*         <li><a href="{{ shopping_cart }}" title="{{ text_shopping_cart }}"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_shopping_cart }}</span></a></li>*/
/*         <li><a href="{{ checkout }}" title="{{ text_checkout }}"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_checkout }}</span></a></li>*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/* </nav>*/
/* <header>*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       <div class="col-sm-5">*/
/*         <div id="logo">{% if logo %}<a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" class="img-responsive" /></a>{% else %}*/
/*           <h1><a href="{{ home }}">{{ name }}</a></h1>*/
/*           {% endif %}</div>*/
/*       </div>*/
/*        <div class="col-sm-3">{{ search }}</div> */
/*       <div class="col-sm-3">{{ cart }}</div>*/
/*     </div>*/
/*   </div>*/
/* </header>*/
/* {{ menu }}*/
/* */
